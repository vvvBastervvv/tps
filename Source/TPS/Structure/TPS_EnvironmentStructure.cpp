// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATPS_EnvironmentStructure::ATPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType1;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial= myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

bool ATPS_EnvironmentStructure::AviableForEffects_Implementation()
{
	UE_LOG(LogTemp,Warning,TEXT("ATPS_EnvironmentStructure::AviableForEffects_Implementation - TEST"));
	return false;
}

// bool ATPS_EnvironmentStructure::AviableForEffectsOnlyCPP()
// {
// 	UE_LOG(LogTemp, Warning, TEXT("ATPS_EnvironmentStructure::AviableForEffectsOnlyCPP - TEST"));
// 	return false;
// }

TArray<UTPS_StateEffect*> ATPS_EnvironmentStructure::GetAllCurrentEffect()
{
	return Effects;
}

void ATPS_EnvironmentStructure::RemoveEffects(UTPS_StateEffect* RemovedEffect)
{

	Effects.Remove(RemovedEffect);
}

void ATPS_EnvironmentStructure::AddEffects(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}


