// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Character/TPSHealthComponent.h"
#include "MyTPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Health, float, Damage);

UCLASS()
class TPS_API UMyTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

	public:
		UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
			FOnShieldChange OnShieldChange;

		FTimerHandle TimerHandle_CollDownShieldTimer;
		FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

		

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float Shield = 100.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
			float ShieldRecovery = 10.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
			float CoolDownShieldRecovery_Time = 5.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
			float ShieldRecoveryValue = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
			float ShieldRecoveryRate = 0.1f;

		 void ChangeCurrentHealth(float ChangeValue) override;

		 float GetCurrentShield();

		 void ChangeShieldValue(float ChangeValue);
		 void CoolDownShieldEnd();
		 void RecoveryShield();

		 UFUNCTION(NetMulticast, RELIABLE)
			 void ShieldEvent_Multicast(float NewShield,float Damage);
			 
};
