// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/TPSGameInstance.h"
#include "Net/UnrealNetwork.h"
#pragma optimize ("", off)

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	/*for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
					WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
				else
				{
					//WeaponSlots.RemoveAt(i);
					//i--;
				}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	}*/
	
}


// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	
		int8 CorrectIndex = ChangeToIndex;
		if (ChangeToIndex > WeaponSlots.Num() - 1)
			CorrectIndex = 0;
		else
			if (ChangeToIndex < 0)
				CorrectIndex = WeaponSlots.Num() - 1;

		FName NewIdWeapon;
		FAdditionalWeaponInfo NewAdditionalInfo;
		int32 NewCurrentIndex = 0;

		if (WeaponSlots.IsValidIndex(CorrectIndex))
		{
			if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
			{
				if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
				{
					//good weapon have ammo start change
					bIsSuccess = true;
				}
				else
				{
					UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
					if (myGI)
					{
						//check ammoSlots for this weapon
						FWeaponInfo myInfo;
						myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								//good weapon have ammo start change
								bIsSuccess = true;
								bIsFind = true;
							}
							j++;
						}
					}
				}
				if (bIsSuccess)
				{
					NewCurrentIndex = CorrectIndex;
					NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
					NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
				}
			}
		}
		if (!bIsSuccess)
		{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			int8 tmpIndex = 0;
			while (iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				iteration++;

				if (bIsForward)
				{
					//Seconditeration = 0;

					tmpIndex = ChangeToIndex + iteration;
				}
				else
				{
					Seconditeration = WeaponSlots.Num() - 1;

					tmpIndex = ChangeToIndex - iteration;
				}

				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
						{
							//WeaponGood
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
									NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of LEFT of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
									NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//WeaponGood
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
											NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with ammo need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					if (bIsForward)
					{
						Seconditeration++;
					}
					else
					{
						Seconditeration--;
					}

				}
			}
		}
		if (bIsSuccess)
		{
			SetAdditionalInfoWeapon(OldIndex, OldInfo);
			OnSwitchWeapon_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
			//OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		}

		return bIsSuccess;
		/*bool bIsSuccess = false;
		int8 CorrectIndex = ChangeToIndex;
		if (ChangeToIndex > WeaponSlots.Num() - 1)
			CorrectIndex = 0;
		else
			if (ChangeToIndex < 0)
				CorrectIndex = WeaponSlots.Num() - 1;

		FName NewIdWeapon;
		FAdditionalWeaponInfo NewAdditionalInfo;
		int32 NewCurrentIndex = 0;

		if (WeaponSlots.IsValidIndex(CorrectIndex))
		{
			if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
			{
				if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
				{
					//good weapon have ammo start change
					bIsSuccess = true;
				}
				else
				{
					UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
					if (myGI)
					{
						//check ammoSlots for this weapon
						FWeaponInfo myInfo;
						myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								//good weapon have ammo start change
								bIsSuccess = true;
								bIsFind = true;
							}
							j++;
						}
					}
				}
				if (bIsSuccess)
				{
					NewCurrentIndex = CorrectIndex;
					NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
					NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
				}
			}
		}

		if (!bIsSuccess)
		{
			if (bIsForward)
			{
				int8 iteration = 0;
				int8 Seconditeration = 0;
				while (iteration < WeaponSlots.Num() && !bIsSuccess)
				{
					iteration++;
					int8 tmpIndex = ChangeToIndex + iteration;
					if (WeaponSlots.IsValidIndex(tmpIndex))
					{
						if (!WeaponSlots[tmpIndex].NameItem.IsNone())
						{
							if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
								NewCurrentIndex = tmpIndex;
							}
							else
							{
								FWeaponInfo myInfo;
								UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
										NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
										NewCurrentIndex = tmpIndex;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
					else
					{
						//go to end of right of array weapon slots
						if (OldIndex != Seconditeration)
						{
							if (WeaponSlots.IsValidIndex(Seconditeration))
							{
								if (!WeaponSlots[Seconditeration].NameItem.IsNone())
								{
									if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
										NewCurrentIndex = Seconditeration;
									}
									else
									{
										FWeaponInfo myInfo;
										UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlots.Num() && !bIsFind)
										{
											if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
											{
												//WeaponGood
												bIsSuccess = true;
												NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
												NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
												NewCurrentIndex = Seconditeration;
												bIsFind = true;
											}
											j++;
										}
									}
								}
							}
						}
						else
						{
							//go to same weapon when start
							if (WeaponSlots.IsValidIndex(Seconditeration))
							{
								if (!WeaponSlots[Seconditeration].NameItem.IsNone())
								{
									if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
									{
										//WeaponGood, it same weapon do nothing
									}
									else
									{
										FWeaponInfo myInfo;
										UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlots.Num() && !bIsFind)
										{
											if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
											{
												if (AmmoSlots[j].Cout > 0)
												{
													//WeaponGood, it same weapon do nothing
												}
												else
												{
													//Not find weapon with amm need init Pistol with infinity ammo
													UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
												}
											}
											j++;
										}
									}
								}
							}
						}
						Seconditeration++;
					}
				}
			}
			else
			{
				int8 iteration = 0;
				int8 Seconditeration = WeaponSlots.Num() - 1;
				while (iteration < WeaponSlots.Num() && !bIsSuccess)
				{
					iteration++;
					int8 tmpIndex = ChangeToIndex - iteration;
					if (WeaponSlots.IsValidIndex(tmpIndex))
					{
						if (!WeaponSlots[tmpIndex].NameItem.IsNone())
						{
							if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
								NewCurrentIndex = tmpIndex;
							}
							else
							{
								FWeaponInfo myInfo;
								UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
										NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
										NewCurrentIndex = tmpIndex;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
					else
					{
						//go to end of LEFT of array weapon slots
						if (OldIndex != Seconditeration)
						{
							if (WeaponSlots.IsValidIndex(Seconditeration))
							{
								if (!WeaponSlots[Seconditeration].NameItem.IsNone())
								{
									if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
										NewCurrentIndex = Seconditeration;
									}
									else
									{
										FWeaponInfo myInfo;
										UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlots.Num() && !bIsFind)
										{
											if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
											{
												//WeaponGood
												bIsSuccess = true;
												NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
												NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
												NewCurrentIndex = Seconditeration;
												bIsFind = true;
											}
											j++;
										}
									}
								}
							}
						}
						else
						{
							//go to same weapon when start
							if (WeaponSlots.IsValidIndex(Seconditeration))
							{
								if (!WeaponSlots[Seconditeration].NameItem.IsNone())
								{
									if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
									{
										//WeaponGood, it same weapon do nothing
									}
									else
									{
										FWeaponInfo myInfo;
										UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlots.Num() && !bIsFind)
										{
											if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
											{
												if (AmmoSlots[j].Cout > 0)
												{
													//WeaponGood, it same weapon do nothing
												}
												else
												{
													//Not find weapon with amm need init Pistol with infinity ammo
													UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
												}
											}
											j++;
										}
									}
								}
							}
						}
						Seconditeration--;
					}
				}
			}
		}

		if (bIsSuccess)
		{
			SetAdditionalInfoWeapon(OldIndex, OldInfo);

			OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
			//OnWeaponAmmoAviable.Broadcast();
		}


		return bIsSuccess;*/
	

}

void UTPSInventoryComponent::OnSwitchWeapon_OnServer_Implementation(FName NewIdWeapon, FAdditionalWeaponInfo NewAdditionalInfo, int32 NewCurrentIndex)
{
	OnSwitchWeapon_Multicast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	
	
}

void UTPSInventoryComponent::OnSwitchWeapon_Multicast_Implementation(FName NewIdWeapon, FAdditionalWeaponInfo NewAdditionalInfo, int32 NewCurrentIndex)
{
	OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

	return result;
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

FName UTPSInventoryComponent::GetWeaponNameBySlotIndex(int32 indexSlot)
{
	FName result;

	if (WeaponSlots.IsValidIndex(indexSlot))
	{
		result = WeaponSlots[indexSlot].NameItem;
	}
	return result;
}


void UTPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}




void UTPSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;
		if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;

		AmmoChangeEvent_Multycast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			//OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}
		i++;
	}
}

bool UTPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int16& AviableAmmoForWeapon)
{
	AviableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoForWeapon = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
			{
				//OnWeaponAmmoAviable.Broadcast(TypeWeapon);//remove not here, only when pickUp ammo this type, or swithc weapon
				return true;
			}

		}

		i++;
	}

	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);//visual empty ammo slot

	return false;
}

bool UTPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
			result = true;
		i++;
	}
	return result;
}

bool UTPSInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UTPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar)//, FDropItem& DropItemInfo)
{
	bool result = false;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot))//, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);

		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		result = true;
	}
	return result;
}

void UTPSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor,FWeaponSlot NewWeapon)
{
	int32 indexSlot = -1;
	int8 i = 0;
	bool Check = false;
	while (i < WeaponSlots.Num())
	{
		if (WeaponSlots[i].NameItem == NewWeapon.NameItem)
		{

			Check = true;
			i++;
			//TryGetWeaponToInventory_Multicast(false);
			//return false;
		}
		else
		{
			i++;
		}
	}

	if (CheckCanTakeWeapon(indexSlot) && !Check)
	{
		if (WeaponSlots.IsValidIndex(indexSlot))
		{
			WeaponSlots[indexSlot] = NewWeapon;

			OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			if(PickUpActor)
			{
				PickUpActor->Destroy();
			}
			// TryGetWeaponToInventory_Multicast(true);
			//return true;
		}
	}
	//return false;
	//TryGetWeaponToInventory_Multicast(false);
}

//bool UTPSInventoryComponent::TryGetWeaponToInventory_Multicast_Implementation(bool result)
//{
//	return result;
//}

bool UTPSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot)//, FDropItem& DropItemInfo)
{
	bool result = true;

	/*FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}
	*/
	return result;
}

bool UTPSInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;
}

bool UTPSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

void UTPSInventoryComponent::AmmoChangeEvent_Multycast_Implementation(EWeaponType TypeWeapon, int32 Count)
{
	OnAmmoChange.Broadcast(TypeWeapon, Count);
}

void UTPSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& WeaponSlotInfo, const TArray<FAmmoSlot>& AmmoSlotsInfo)
{
	WeaponSlots = WeaponSlotInfo;
	AmmoSlots = AmmoSlotsInfo;
	//for (int8 i = 0; i < WeaponSlots.Num(); i++)
	//{
	//	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	//	if (myGI)
	//	{
	//		if (!WeaponSlots[i].NameItem.IsNone())
	//		{
	//			/*FWeaponInfo Info;
	//			if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
	//				WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
	//			else
	//			{
	//				//WeaponSlots.RemoveAt(i);
	//				//i--;
	//			}*/
	//		}
	//	}
	//}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{

		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			//OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	}
}

void UTPSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UTPSInventoryComponent, AmmoSlots);
	



}

#pragma optimize ("", on)