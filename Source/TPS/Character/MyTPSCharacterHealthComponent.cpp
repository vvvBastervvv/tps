// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/MyTPSCharacterHealthComponent.h"

void UMyTPSCharacterHealthComponent::ChangeCurrentHealth(float ChangeValue)
{


	//for char
	
 float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		
		if (Shield < 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("UMyTPSCharacterHealthComponent::ChangeCurrentHealth - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeCurrentHealth(ChangeValue);
	}

}

float UMyTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldEvent_Multicast(Shield, ChangeValue);
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld() )
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer,this,&UMyTPSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecovery_Time,false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	
}

void UMyTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld() )
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
	
}

void UMyTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld() )
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	UE_LOG(LogTemp, Warning, TEXT("Bla"));
	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
	ShieldEvent_Multicast(Shield, ShieldRecoveryValue);
	
}

void UMyTPSCharacterHealthComponent::ShieldEvent_Multicast_Implementation(float NewShield, float Damage)
{
	OnShieldChange.Broadcast(NewShield, Damage);
}
