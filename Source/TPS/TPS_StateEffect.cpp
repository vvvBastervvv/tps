// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_StateEffect.h"
#include "Character/TPSHealthComponent.h"
#include "Interface/TPS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UTPS_StateEffect::InitObject(AActor* Actor)//(APawn* Pawn)
{

	myActor = Actor;

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffects(this);
	}
	//GetClass()
	//UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect::InitObject - test"));
	return true;
}

// void UTPS_StateEffect::ExecuteEffect(float DeltaTime)
// {
// 
// }

void UTPS_StateEffect::DestroyObject()
{
	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffects(this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTPS_StateEffect::CheclStakebleEffect()
{
	return false;
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth(Power);
		}
	}
	DestroyObject();
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		
		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, "Spine", Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		
	}

	
	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
		Super::DestroyObject();
	}
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth(Power);
		}
		if (RadiusAoeMax > 0.0f && RadiusAoeMin > 0.0f)
		{
			TArray<AActor*> IgnoredActor;
			UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
				DamageAoeMax,
				DamageAoeMax * 0.2f,
				myActor->GetActorLocation(),
				RadiusAoeMin,
				RadiusAoeMax,
				5.0f,
				NULL, IgnoredActor, myActor, nullptr);
		}
	}
}
