// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TPS/TPS.h"
#include "TPS/Interface/TPS_IGameActor.h"
//#include "PhysicalMaterials/PhysicalMaterial.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor,TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{

	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		
			UTPS_StateEffect* myEffect = Cast<UTPS_StateEffect>(AddEffectClass->GetDefaultObject());
			if (myEffect)
			{
				bool bIsCanAdd = false;
				int8 i = 0;
				while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
				{
					if (myEffect->PossibleInteractSurface[i] == SurfaceType)
					{
						bool bisCanAddEffect = false;
						bIsCanAdd = true;
						if (!myEffect->bisStakable)
						{
							int8 j = 0;
							TArray<UTPS_StateEffect*> CurrentEffects;
							ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(TakeEffectActor);
							if (myInterface)
							{
								CurrentEffects = myInterface->GetAllCurrentEffect();
							}

							if (CurrentEffects.Num()>0)
							{
								while (j < CurrentEffects.Num() && !bisCanAddEffect)
								{
									if (CurrentEffects[j]->GetClass() != AddEffectClass)
									{
										bisCanAddEffect = true;
									}
									j++;
								}
							}
							else
							{
								bisCanAddEffect = true;
							}

							
						}
						else
						{
							bisCanAddEffect = true;

						}
						if (bisCanAddEffect)
						{
							//bIsCanAdd = true;
							UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(TakeEffectActor, AddEffectClass);
							if (NewEffect)
							{
								NewEffect->InitObject(TakeEffectActor);
							}
						}
						
					}
					i++;
				}
			}
		
	}
}
